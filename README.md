# Open Mobility Indicators – Notebooks

## Installation

Some dependencies like pyrosm or osmnx are more easily installable with `conda` than with `pip`.

Using `conda` without modifying your user environment is possible with `pyenv`.

Prerequisites: [pyenv](https://github.com/pyenv/pyenv)

```bash
# Install miniconda3-latest Python version with pyenv:
pyenv install miniconda3-latest

# Create and activate a virtualenv named test-conda-1 from the miniconda3-latest Python miniconda3-latest:
pyenv virtualenv miniconda3-latest test-conda-1
pyenv activate test-conda-1

# Your shell should be prepended by `(test-conda-1)`.

# The `conda` command is now in the PATH of your shell.

# Configure "conda-forge" external source:
conda config --prepend channels conda-forge

# Create a new conda environment named "indicator-notebooks":
conda create -n indicator-notebooks --strict-channel-priority

# This "indicator-notebooks" conda env sits in the "test-conda-1" virtualenv.

# Source the conda.sh file (once during the shell session) to avoid failing at the next step:
# (OR use `conda init bash` to append a config block to your shell config file)
. ~/.pyenv/versions/miniconda3-latest/etc/profile.d/conda.sh

# Your shell should be prepended by `(indicator-notebooks)` and `(test-conda-1)`.

# Activate the "indicator-notebooks" conda environment:
conda activate indicator-notebooks

# Install project dependencies:
conda env update --file environment.yml

# environment.yml contains the dependencies of the project (deep and frozen).

# To install other packages:
conda install <package_name>
# Then regenerate environment.yml (if the added package is intended to stay):
conda env export > environment.yml
```

Then run jupyter lab:

```bash
jupyter lab
```

When you're done working with the project, you may deactivate the virtualenvs like this:

```bash
# To deactivate the conda env:
conda deactivate

# To deactivate the test-conda-1 virtualenv:
pyenv deactivate
```

## Notebooks

Each notebook is stored in a sub-directory of [`notebooks`](./notebooks) and must follow some conventions regarding its input parameters, output files and other companion files.

Those conventions allow the automatic workflow to compute all the notebooks at once.

### Notebook name

The name of the notebook should be the same as the sub-directory. For example: `./notebooks/omi_score/omi_score.ipynb`.

### Input parameters

The following parameters are common to all the notebooks. They are designed to be overloaded by [Papermill](https://papermill.readthedocs.io/en/latest/) for batch notebook execution.

- `debug` (`bool`): if `True`, the cells starting with `if debug:` will be computed
- `download_data` (`bool`): if `True`, the notebook should start by downloading the data it needs to run
- `location` (`str`): for notebooks that use the Overpass API, defines the location to process
- `output_geojson` (`str`): file path of the generated GeoJSON file

### GeoJSON feature properties

The features of the GeoJSON output file can have special properties that will be interpreted in the frontend.

- `color` (`str`): the color of the feature

### Popup template

The frontend displays a popup when clicking on a GeoJSON feature. The content of the popup can be defined besides the notebook file, in a file called `popup.template` (TODO update the name when the template engine will be chosen).

Available substitution variables:

- every GeoJSON feature property

Example:

```text
The score is {score}!
```
