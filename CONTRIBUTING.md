# Contributing

## How to add a new indicator layer to the app?

An indicator layer is produced by a Jupyter Notebook that will be run by the infrastructure of Open Mobility Indicators. The output of that notebook will be visible on the map as long as it respects those rules:

- A notebook comes with other files. They must be stored in a sub-directory named after the indicator ID, like `./notebooks/{indicator_id}`. For example, the `dead_ends` indicator is stored in `./notebooks/dead_ends/`.
- The notebook itself must be named like `{indicator_id}.ipynb`.
- If a notebook use external Python dependencies, they must be declared in an environment file named `environment.yml`.
  > Note: we noticed that some geospatial libraries are easily installable from Conda because binary packages exist, and not for PyPI. As a consequence we decided to use Conda environments.
- An indicator layer must be described by a meta-data file named `indicator.yaml`.
- The notebook must write a GeoJSON file named `{indicator_id}.geojson`

Once your notebook is ready, you can submit a merge request on this repository. The Open Mobility Indicators team will review it, merge it, run your indicator notebook and deploy the output to the map frontend.

Read the following sections for more details.

## Notebook parameters

When deploying the indicator to production, the notebook will be run by [Papermill](https://papermill.readthedocs.io/en/latest/) (a tool for parameterizing and executing Jupyter Notebooks) with the following parameters:

- `debug` (`bool`): `False`
- `download_data` (`bool`): `True`
- `location` (`str`): the name of a boundary that must be found by [Nominatim](https://nominatim.openstreetmap.org/ui/search.html) (e.g. `Provence-Alpes-Côte d'Azur, France`)
- `output_dir` (`str`): the path of the directory where to store the output file

As a consequence you must provide a notebook cell (the first in general) with those Python variables.
You can set them to default values that suit your workflow best, while allowing Papermill to overwrite them at execution.

You must write the output file to `{output_dir}/{indicator_id}.geojson`.

For example, see how it's done in the `dead_ends` or `omi_score` notebooks.

See also: <https://papermill.readthedocs.io/en/latest/usage-parameterize.html>

## How to freeze dependencies?

If your notebook needs dependencies, you must freeze them into an `environment.yml`. This file will be used to install those dependencies automatically when running the notebook.

Pre-requisite: have the "conda" command installed. Follow [Conda docs](https://docs.conda.io/en/latest/miniconda.html) if necessary.

Here are the basic steps to follow to work with a Conda environment:

```bash
# Configure "conda-forge" external source, because it contains many known packages
conda config --prepend channels conda-forge

# Create a new conda environment named "indicator-notebooks"
conda create -n indicator-notebooks --strict-channel-priority

conda activate indicator-notebooks

# Optional: apply an existing environment.yml file
conda env update --file environment.yml

# Install manually the packages you want
conda install <package_name>

# Freeze dependencies in environment.yml:
conda env export > environment.yml
```

## indicator.yaml

This file is used by the map frontend to render the indicator selector (in the bottom-right corner of the map) with rich text.

The description can be Markdown text.

```yaml
indicator:
  name: My indicator
  description: |
    This indicator shows the number of ... that ...

    See [this link](https://example.com/) ...
```

## Format of the GeoJSON output

The `{indicator_id}.geojson` output file will be used by the Open Mobility Indicators computing pipeline (cf [omi-ops](https://gitlab.com/open-mobility-indicators/omi-ops/)) in order to produce the vector tiles to display on the map.

If you work with a `GeoDataFrame` from [GeoPandas](https://geopandas.org/) you can serialize it to GeoJSON, as we do in the `dead_ends` and `omi_score` notebooks.

### Color property

The GeoJSON features can also have a `color` property. In that case, it will be used by the map frontend to style the rendering. When working with GeoPandas, this can be done by adding a column to the `GeoDataFrame`: it will be serialized as a GeoJSON property on the feature.

For example, in the `dead_ends` notebook, we add a `color` column on the `GeoDataFrame`.
